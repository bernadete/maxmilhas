const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');

const Joi = BaseJoi.extend(Extension);


const schemaPayloadIntencaoBusca = Joi.object().keys({
  tripType: Joi.string().valid(['RT', 'OW']).required(),
  from: Joi.string().required(),
  to: Joi.string().required(),
  adults: Joi.number().min(1).max(9).required(),
  children: Joi.number().optional().allow('', null),
  infants: Joi.number().optional().allow('', null),
  outboundDate: Joi.date().format('YYYY-MM-DD').required(),
  inboundDate: Joi.date().format('YYYY-MM-DD'),
  cabin: Joi.string().valid(['EC', 'EX']).optional().allow('', null)
});


const schemaResponse = Joi.object().keys({
  id: Joi.string().required(),
  createdDate: Joi.string().required(),
  legacyId: Joi.string().optional(),
  isInternational: Joi.boolean(),
  isMercosul: Joi.boolean(),
  fromCountry: Joi.string(),
  toCountry: Joi.string(),
  airlines: Joi.array().items(Joi.object({
    label: Joi.string().required(),
    timeout: Joi.number().required(),
    status: Joi.object({
      enable: Joi.boolean().required(),
      message: Joi.string().allow('')
    })
  }))
});


module.exports = {
  schemaPayloadIntencaoBusca,
  schemaResponse
};
