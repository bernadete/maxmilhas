const moment = require('moment');

const buscaUmadultoDoisBebes = {
  tripType: 'RT',
  from: 'CNF',
  to: 'GRU',
  adults: 1,
  children: 1,
  infants: 2,
  outboundDate: '2018-12-27',
  inboundDate: '2019-03-03',
  cabin: 'EC'
};

const busca10Adults = {
  tripType: 'RT',
  from: 'CNF',
  to: 'GRU',
  adults: 10,
  children: 1,
  infants: 2,
  outboundDate: '2018-12-27',
  inboundDate: '2019-03-03',
  cabin: 'EC'
};

const aeroportoPartidaInexistente = {
  tripType: 'RT',
  from: 'AAA',
  to: 'GRU',
  adults: 2,
  children: 1,
  infants: 2,
  outboundDate: '2018-12-27',
  inboundDate: '2019-03-03',
  cabin: 'EC'
};

const aeroportoChegadaInexistente = {
  tripType: 'RT',
  from: 'CNF',
  to: 'AAA',
  adults: 2,
  children: 1,
  infants: 2,
  outboundDate: '2018-12-27',
  inboundDate: '2019-03-03',
  cabin: 'EC'
};

const buscaVooExecutivoAeroportoNacional = {
  tripType: 'RT',
  from: 'CGH',
  to: 'GRU',
  adults: 2,
  children: 1,
  infants: 0,
  outboundDate: moment().add(10, 'days').format('YYYY-MM-DD'),
  inboundDate: moment().add(15, 'days').format('YYYY-MM-DD'),
  cabin: 'EX'
};

const buscaValidarSchema = {
  tripType: 'RT',
  from: 'CGH',
  to: 'GRU',
  adults: 2,
  children: 4,
  infants: 1,
  outboundDate: '2018-12-27',
  inboundDate: '2019-03-03'
};

const buscaDataAtual = {
  tripType: 'RT',
  from: 'CGH',
  to: 'GRU',
  adults: 2,
  children: 1,
  infants: 0,
  outboundDate: moment().format('YYYY-MM-DD'),
  inboundDate: moment().format('YYYY-MM-DD'),
  cabin: 'EX'
};

module.exports = {
  buscaUmadultoDoisBebes,
  busca10Adults,
  aeroportoPartidaInexistente,
  aeroportoChegadaInexistente,
  buscaVooExecutivoAeroportoNacional,
  buscaDataAtual,
  buscaValidarSchema

};
