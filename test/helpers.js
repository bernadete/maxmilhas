const supertest = require('supertest');
const chai = require('chai');


global.request = supertest('https://flight-pricing-hmg.maxmilhas.com.br/search');
global.expect = chai.expect;
global.authorization = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYXhtaWxoYXMuY29tLmJyIiwiaWF0IjoxNTIxNTc4MTg3LCJleHAiOjE1MjQyNTY1ODgsImF1ZCI6InRlc3RlLXFhIiwic3ViIjoidGVzdGUtcWEiLCJlbnYiOiJobWcifQ.-Q04QmHo_Hoy3rq0y3V3hhyIeqdzqTlo27rLkhAOI7s';
