const joiAssert = require('joi-assert');
const request = require('supertest');
const {
  schemaResponse
} = require('../schema/schema.js');

const URL = 'http://www.mocky.io/';
const PATH = 'v2/5ab92da42c0000560018641e';


describe('Contrato API Response', () => {

  it('Validando Response', done => {
    request(URL)
      .get(PATH)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        expect(200);
        joiAssert(res.body, schemaResponse);
        done(err);
      });
  });
});
