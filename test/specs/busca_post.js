const dataBusca = require('../data/data_busca.js');


describe('Serviço: Busca', () => {
  describe('Post - Cenários Falha', () => {
    it('Um adulto com 2 bebês', done => {
      request
        .post('/')
        .set('Authorization', authorization)
        .send(dataBusca.buscaUmadultoDoisBebes)
        .expect(422)
        .end((err, res) => {
          expect(res.body.type).to.equal('AdultsGreaterInfantsValidationError');
          done(err);
        });
    });


    it('10 Adultos', done => {
      request
        .post('/')
        .set('Authorization', authorization)
        .send(dataBusca.busca10Adults)
        .expect(422)
        .end((err, res) => {
          expect(res.body.type).to.equal('PassengersQuantityValidationError');
          done(err);
        });
    });


    it('Aeroporto Partida Inexistente', done => {
      request
        .post('/')
        .set('Authorization', authorization)
        .send(dataBusca.aeroportoPartidaInexistente)
        .expect(404)
        .end((err, res) => {
          expect(res.body.type).to.equal('ResourceNotFoundError');
          done(err);
        });
    });


    it('Aeroporto Chegada Inexistente', done => {
      request
        .post('/')
        .set('Authorization', authorization)
        .send(dataBusca.aeroportoChegadaInexistente)
        .expect(404)
        .end((err, res) => {
          expect(res.body.type).to.equal('ResourceNotFoundError');
          done(err);
        });
    });
  });
});


describe('Serviço: Busca', () => {
  describe('Post - Cenários Sucesso', () => {
    it('Voo executivo para trecho Aeroporto Nacional', done => {
      request
        .post('/')
        .set('Authorization', authorization)
        .send(dataBusca.buscaVooExecutivoAeroportoNacional)
        .expect(200)
        .end((err, res) => {
          expect(res.body.airlines[3].status.message).to.equal('Indisponível para classe executiva');
          done(err);
        });
    });


    it('Data Atual', done => {
      request
        .post('/')
        .set('Authorization', authorization)
        .send(dataBusca.buscaDataAtual)
        .expect(200)
        .end((err, res) => {
          expect(res.body.airlines[1].status.message).to.equal('Somente partidas superiores a 24 horas');
          done(err);
        });
    });
  });
});
