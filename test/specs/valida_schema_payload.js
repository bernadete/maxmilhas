const joiAssert = require('joi-assert');
const dataBusca = require('../data/data_busca.js');
const {
  schemaPayloadIntencaoBusca
} = require('../schema/schema.js');

describe('Contrato API Payload', () => {
  it('Validando Payload - Intenção Busca', done => {
    request
      .post('/')
      .set('Authorization', authorization)
      .send(dataBusca.buscaValidarSchema)
      .expect(200)
      .end((err) => {
        joiAssert(dataBusca.buscaValidarSchema, schemaPayloadIntencaoBusca);
        done(err);
      });
  });
});
