module.exports = {
  "extends": "airbnb-base",
  "env": {
    "es6": true,
    "node": true,
    "mocha": true
  },
  "rules": {
    "no-console": 0,
    "prefer-destructuring": 0,
    "function-paren-newline": 0,
    "no-param-reassign": ["error", {
      "props": false
    }],
    "comma-dangle": ["error", "never"],
    "space-before-function-paren": ["error", "never"],
    "func-names": ["error", "never"],
    "arrow-parens": ["error", "as-needed", {
      "requireForBlockBody": false
    }],
  }
}
