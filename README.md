# README #

Projeto de Testes API de Buscas - MaxMilhas


## Instalacao ##

Passos:
1. git clone git@bitbucket.org:bernadete/maxmilhas.git
2. cd maxmilhas
3. npm install


## Rodar Testes ##
npm run test



## Estrutura de diretorios ##
1. data - Massa de Testes
2. schema - schema
3. specs - arquivos de testes
